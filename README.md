## [Kadıköy Tesisat](https://modatesisat.net)

> Genç ve dinamik ekibimizle hizmetinizdeyiz.

### Hakkımızda

Kadıköy’de hızlı güvenli temiz bir şekilde ev ve işyerlerimizin her türlü tesisatı tadilat ve tamirat işlerini (banyo, musluk, klozet, tıkanıklık giderme ve her türlü su kaçaklarının tespiti vb.) yapıyoruz.

Yılların verdiği tecrübe ve deneyimle sizlere en kaliteli en güvenilir hizmeti vererek yaptığımız işlere verdiğimiz garanti ve güvenle sizlerle çalışmak bize mutluluk verecek. Kadıköy Tesisat, tesisat işlerinin en hızlı en güvenilir olan şirketi.

[modatesisat.net](https://modatesisat.net)

### İletişim

Hızlı bir şekilde destek almak için bizi arayın.

|GSM|E-posta|Adres|
|--|--|--|
|+905318621338|iletisim {et} modatesisat {nokta} net |Osmanağa Mahallesi, Pazar Yolu Sokak, No: 2, Ekizoğlu İş Hanı B: 4, Kadıköy/İSTANBUL|
