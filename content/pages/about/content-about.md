---
title: 'Kadıköy Tesisat'
weight: 1
date: 2018-12-06T09:29:16+10:00
background: ''
align: right
button: 'İLETİŞİME GEÇ'
buttonLink: 'contact'
---

Kadıköy’de hızlı güvenli temiz bir şekilde ev ve işyerlerimizin her türlü tesisatı tadilat ve tamirat işlerini (banyo, musluk, klozet, tıkanıklık giderme ve her türlü su kaçaklarının tespiti vb.) yapıyoruz.

Yılların verdiği tecrübe ve deneyimle sizlere en kaliteli en güvenilir hizmeti vererek yaptığımız işlere verdiğimiz garanti ve güvenle sizlerle çalışmak bize mutluluk verecek. Kadıköy Tesisat, tesisat işlerinin en hızlı en güvenilir olan şirketi.
