---
title: 'Hakkımızda'
date: 2018-12-06T09:29:16+10:00
layout: 'about'
heroHeading: 'Hakkımızda'
heroSubHeading: "Genç ve dinamik ekibimizle hizmetinizde."
heroBackground: 'images/1.jpeg'
---

<div>
{{< content-strip-left "/pages/about" "content1" >}}
</div>
<div>
{{< content-strip-right "/pages/about" "content2" >}}
</div>
<div>
{{< content-strip-center "/pages/about" "content3" >}}
</div>
