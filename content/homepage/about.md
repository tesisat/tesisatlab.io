---
title: 'Ne yapıyoruz?'
weight: 1
background: ''
button: 'Daha fazla'
buttonLink: 'about'
---

Kadıköy'de hızlı güvenli temiz bir şekilde ev ve işyerlerimizin her türlü tesisatı tadilat ve tamirat işlerini (banyo, musluk, klozet, tıkanıklık giderme ve her türlü su kaçaklarının tespiti vb.) yapıyoruz.
